FROM ubuntu:latest
RUN apt-get update && apt-get install -y nginx
WORKDIR /var/www/html
COPY static-web/* .
EXPOSE 80/tcp
CMD [ "nginx", "-g daemon off;"]